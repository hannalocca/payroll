<?php 
class PayrollModel extends CI_Model
{
		public function addCutoff($from_,$date_from,$to_,$date_to,$year){
			$info = array('from_' => $from_,'date_from'=>$date_from,'to_'=>$to_,'date_to'=>$date_to,'year'=>$year);
			$this->db->insert('cutoff',$info);
		}
		public function selectExistCutoff($from_,$date_from,$to_,$date_to,$year){
			$info = array('from_' => $from_,'date_from'=>$date_from,'to_'=>$to_,'date_to'=>$date_to,'year'=>$year);
			$this->db->where($info);
			return $this->db->get('cutoff')->result();
		}
		public function get(){
			return $this->db->get('cutoff')->result();
		}
		public function selectEmployeetoCompute($cutoff_id){
			return $this->db->query("SELECT a.employee_id,CONCAT(a.Lastname,', ',a.Firstname,' ',a.Middlename,' ',a.Suffix,' / ',a.Department ) as fullname FROM jp_payroll.employee a WHERE a.Status='Employee' AND NOT EXISTS(SELECT * FROM jp_payroll.payroll c WHERE c.employee_id=a.employee_id AND c.cutoff_id='$cutoff_id') ORDER BY a.Department")->result();
		}

		public function selectSpecificEmployeetoCompute($id){
			return $this->db->query("SELECT a.*,b.* FROM employee a LEFT JOIN deduction b ON a.Employee_id = b.Employee_id WHERE a.Employee_Id='$id'")->result();

		}

		public function savePayslip($prateperdays,$pnumberofdays,$povertimeamount,$pallowance,$padjustments,$pgrosspay,$ptotalgrosspay,$absentamount,$pundertimeamount,$pcashadvance,$psss,$pphilhealth,$ppagibig,$ptax,$ptotaldeductions,$pnetpay,$employee_id,$cutoff_id,$absentday,$othour,$otmin,$uthour,$utmin){
			$info = array('prateperdays' => $prateperdays,
						  'pnumberofdays' => $pnumberofdays,
						  'povertimeamount' => $povertimeamount,
						  'pallowance' => $pallowance,
						  'padjustments' => $padjustments,
						  'pgrosspay' => $pgrosspay,
						  'ptotalgrosspay' => $ptotalgrosspay,
						  'absentamount' => $absentamount,
						  'pundertimeamount' => $pundertimeamount,
						  'pcashadvance' => $pcashadvance,
						  'psss' => $psss,
						  'pphilhealth' => $pphilhealth,
						  'ppagibig' => $ppagibig,
						  'ptax' => $ptax,
						  'ptotaldeductions' => $ptotaldeductions,
						  'pnetpay' => $pnetpay,
						  'employee_id' => $employee_id,
						  'cutoff_id' => $cutoff_id,
						  'absentday' => $absentday,
						  'othour' => $othour,
						  'otmin' => $otmin,
						  'uthour' => $uthour,
						  'utmin' => $utmin
						   );
			$this->db->insert('payroll',$info);
		}
		public function selectPayrolled($id){
			return $this->db->query("SELECT a.payroll_id,b.Firstname , b.Middlename, b.Lastname,b.Suffix,b.Position,b.Department FROM payroll a LEFT JOIN employee b ON a.employee_id = b.employee_id WHERE a.cutoff_id = '$id'")->result();
		}
		public function selectPayrolledfinal($id,$cat,$branch){
			return $this->db->query("SELECT a.*,b.*,c.*,d.* FROM jp_payroll.payroll a LEFT JOIN jp_payroll.employee b ON a.employee_id = b.employee_id
									LEFT JOIN jp_payroll.cutoff c ON c.cutoff_id = a.cutoff_id
									LEFT JOIN jp_payroll.branch d ON b.Branch = d.branch_id WHERE a.cutoff_id = '$id' AND b.Department='$cat' AND b.Branch='$branch'")->result();
		}
		public function selectPayrolledfinalSpecific($id){
			return $this->db->query("SELECT a.*,b.*,c.* FROM jp_payroll.payroll a LEFT JOIN jp_payroll.employee b ON a.employee_id = b.employee_id
									LEFT JOIN jp_payroll.cutoff c ON c.cutoff_id = a.cutoff_id WHERE a.payroll_id = '$id'")->result();
		}
		public function accountnumbers($id,$cat,$branch){
			return $this->db->query("SELECT a.*,b.*,c.*,d.* FROM employee a LEFT JOIN payroll b ON b.employee_id = a.employee_id
			LEFT JOIN cutoff c ON b.cutoff_id = c.cutoff_id 
			LEFT JOIN jp_payroll.branch d ON a.Branch = d.branch_id WHERE b.cutoff_id = '$id' AND a.Department='$cat' AND a.Branch='$branch' ORDER BY a.Department ASC")->result();
		}
		public function accountnumbersSummary($id,$cat,$branch){
			return $this->db->query("SELECT a.*,b.*,c.*,d.* FROM employee a LEFT JOIN payroll b ON b.employee_id = a.employee_id
			LEFT JOIN cutoff c ON b.cutoff_id = c.cutoff_id
			LEFT JOIN branch d ON a.Branch = d.branch_id WHERE b.cutoff_id = '$id' AND a.Department='$cat' AND a.Branch='$branch' ORDER BY a.Firstname ASC")->result();
		}
		public function deleteFrompayslips($id){
			$this->db->where('payroll_id',$id);
			$this->db->delete('payroll');
		}

		public function deleteCutfoff($cutoff_id){
			$this->db->where('cutoff_id',$cutoff_id);
			$this->db->delete('cutoff');
		}
		public function deleteCutfoffpayroll($cutoff_id){
			$this->db->where('cutoff_id',$cutoff_id);
			$this->db->delete('payroll');
		}
}
?>