<?php 
class EmployeeModel extends CI_Model
{
	public function saveEmployee($emp_id,$fname,$mname,$lname,$suffix,$id,$birth,$gender,$address,$zip,$mstatus,$contact,$position,$salary,$rate,$hired,$sssnum,$philnum,$pagnum,$sssamount,$philamount,$pagamount,$taxamount,$taxnum,$allowance,$bank,$department,$branch){
		
		if($emp_id==""){
			$info = array('Firstname' => $fname , 
			'Middlename' => $mname,
			'Lastname' =>$lname,
			'Suffix'=>$suffix,
			'Identification_number'=>$id,
			'Birthdate' =>$birth,
			'Gender'=>$gender,
			'Address'=>$address,
			'Zip_code'=>$zip,
			'Marital_status'=>$mstatus,
			'Contact'=>$contact,
			'Department'=>$department,
			'Branch'=>$branch,
			'Position'=>$position,
			'Salary'=>$salary,
			'Rate_perday'=>$rate,
			'Hired_date'=>$hired,
			'Allowance'=>$allowance,
			'Bank_account_number'=>$bank
		);
		$this->db->insert('employee',$info);
		$insert_id = $this->db->insert_id();
		$deduction = array('SSS_number' => $sssnum , 
			'PHILHEALTH_number' => $philnum,
			'PAGIBIG_number' =>$pagnum,
			'SSS_amount'=>$sssamount,
			'PHILHEALTH_amount'=>$philamount,
			'PAGIBIG_amount' =>$pagamount,
			'TAX'=>$taxamount,
			'TIN_number'=>$taxnum,
			'Employee_id'=>$insert_id,);
		$this->db->insert('deduction',$deduction);
		}
		else{
			$info = array('Firstname' => $fname , 
			'Middlename' => $mname,
			'Lastname' =>$lname,
			'Suffix'=>$suffix,
			'Identification_number'=>$id,
			'Birthdate' =>$birth,
			'Gender'=>$gender,
			'Address'=>$address,
			'Zip_code'=>$zip,
			'Marital_status'=>$mstatus,
			'Contact'=>$contact,
			'Department'=>$department,
			'Branch'=>$branch,
			'Position'=>$position,
			'Salary'=>$salary,
			'Rate_perday'=>$rate,
			'Hired_date'=>$hired,
			'Allowance'=>$allowance,
			'Bank_account_number'=>$bank
			);
			$where = array('Employee_id' => $emp_id );
			$this->db->update('employee',$info,$where);
			$deduction = array('SSS_number' => $sssnum , 
			'PHILHEALTH_number' => $philnum,
			'PAGIBIG_number' =>$pagnum,
			'SSS_amount'=>$sssamount,
			'PHILHEALTH_amount'=>$philamount,
			'PAGIBIG_amount' =>$pagamount,
			'TAX'=>$taxamount,
			'TIN_number'=>$taxnum);
			$this->db->update('deduction',$deduction,$where);
		}
	}
	public function get(){
		return $this->db->get('employee')->result();
	}
	public function getSpecific($id){
		return $this->db->query("SELECT a.*,b.*,c.* FROM employee a LEFT JOIN deduction b ON a.Employee_id = b.Employee_id 
								LEFT JOIN branch c ON c.branch_id = a.Branch WHERE a.Employee_Id='$id'")->result();
	}
	public function deletes($id){
		$this->db->where('Employee_id',$id);
		$this->db->delete('employee');
	}public function deletesd($id){
		$this->db->where('Employee_id',$id);
		$this->db->delete('deduction');
	}
	public function changeEmpStatus($id,$stat){
		$info = array('Status' => $stat);
		$where = array('Employee_id' => $id);
		$this->db->update('employee',$info,$where);
	}
}
?>