<?php 
 class SettingsModel extends CI_Model
 {
 	public function selectBranches(){
 		return $this->db->get('branch')->result();
 	}
 	public function saveBranch($branch){
 		$info = array('branch_name' => $branch);
 		$this->db->insert('branch',$info);
 	}
 }
?>