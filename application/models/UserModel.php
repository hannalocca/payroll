<?php 
class UserModel extends CI_Model
{
	public function Login($user,$password){
		return $this->db->query("SELECT a.*,b.* FROM employee a LEFT JOIN user b ON a.Employee_id = b.Employee_id WHERE b.username='$user' AND b.password = '$password'")->row_array();
	}
	public function selectEmployeeforAddUsers(){
			return $this->db->query("SELECT a.Position,a.employee_id,CONCAT(a.Lastname,', ',a.Firstname,' ',a.Middlename,' ',a.Suffix,' .' ) as fullname FROM jp_payroll.employee a WHERE a.Status='Employee'")->result();
		}
	public function saveUser($employee_id,$usertype,$username,$password){
		$info = array('Employee_id' =>$employee_id ,'username'=>$username,'password'=>$password,'user_type'=>$usertype);
		$this->db->insert('user',$info);
	}
	public function selectUsers(){
		return $this->db->query("SELECT a.*,b.* FROM user a LEFT JOIN employee b ON a.employee_id = b.employee_id")->result();
	}
	public function deleteUser($id){
		$this->db->where('user_id',$id);
		$this->db->delete('user');
	}
	public function isActive($id,$stats){
		$where = array('user_id'=>$id);
		$info = array('isActive'=>$stats);
		$this->db->update('user',$info,$where);
	}
	public function selectusernameifexist($username){
		$this->db->where('username',$username);
		return $this->db->get('user')->result();
	}
}
?>