<div class="sidebar"  data-image="<?php echo base_url()?>assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

        <div class="sidebar-wrapper" style="background-color: #4267b2 !important; "> 
            <div class="logo">
                <center>
                    <h4 class="title titlename profilename"></h4>
                    <h5 class="nametitle profiletitle"></h5>
                </center>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url()?>">
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url()?>Employee">
                        <p>Employee Information</p>
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url()?>Payroll">
                        <p>Payroll</p>
                    </a>
                </li>
                <li class="btndrop">
                    <a href="#">
                        <p>Settings</p>
                    </a>
                        <ul class="nav droped">
                            <li>
                                <a href="<?php echo base_url()?>Settings/Branches">
                                    <p class="pull-right">Add Branch</p>
                                </a>
                            </li>
                                
                        </ul>
                </li>
                <li class="btndropreports">
                    <a href="#"><p>Reports</p></a>
                    <ul class="nav dropedsumrep">
                                    <li class="btndropreportssumry">
                                        <a href="<?php echo base_url()?>Payroll/PayrollSummary">
                                            <p >Payroll Summary</p>
                                        </a>
                                    </li> 
                                    <li class="btndropreportssumry">
                                        <a href="<?php echo base_url()?>Payroll/PrintPayslip">
                                            <p >Payslips</p>
                                        </a>
                                    </li> 
                                    <li class="btndropreportssumry">
                                        <a href="<?php echo base_url()?>Payroll/AccNum">
                                            <p >Account Numbers</p>
                                        </a>
                                    </li>
                                </ul>
                </li>
                <!-- <li class="">
                    <a href="<?php echo base_url()?>addUser">
                        <p>Add User</p>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.droped').hide();
            $('.dropedsumrep').hide();
            $('.btndrop').hover(function(){
                 $('.droped').show();
            },function(){
                $('.droped').hide();
            });

            $('.btndropreports').hover(function(){
                $('.dropedsumrep').show();
            },function(){
                $('.dropedsumrep').hide();
            });

            $('.btndropreportssumry').hover(function(){
                $('.btndropreportssumrysub').show();
            },function(){
            });
        });
    </script>