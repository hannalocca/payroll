<!DOCTYPE html>
<html lang="en">
<head>
  <title>JP Payroll - Login</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <!-- <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/assets/images/icons/favicon.ico"/> -->
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/animate/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/css/util.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweet-alert/sweetalert.css">
<!--===============================================================================================-->
</head>
<body style="background-image: url(<?php echo base_url()?>uploads/jp_bg.jpg);height: 600px;background-repeat: no-repeat;background-size: cover;background-attachment: fixed;background-position: center;">
  <div class="limiter ">
    <div class="container-login100" style="background: none !important;">

      <div class="wrap-login100" style="opacity: 0.9;">
        <div class="login100-form-title" >
          
          <span class="login100-form-title-1 loginform">
            JaPhil Global Coins Corp. Payroll  
          </span>
        </div>
        <form class="login100-form validate-form loginform" id="loginjud" autocomplete="off" method="post">
          <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
            <span class="label-input100">Username</span>
            <input class="input100" type="text" name="username" id="luser" placeholder="Enter username">
            <span class="focus-input100"></span>
          </div>

          <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
            <span class="label-input100">Password</span>
            <input class="input100" type="password" id="lpass" name="password" placeholder="Enter password">
            <span class="focus-input100"></span>
          </div>

          <div class="flex-sb-m w-full p-b-30">
            

            <!-- <div>
              <a href="#" class="txt1">
                Forgot Password?
              </a>
            </div> -->
             <div>
            </div>
          </div>
            <button type="submit" class="btn btn-success" style="margin: 12px;width: 70%;background-color:red;border:0;color:white;">Login</button>

        </form>

      </div>
      
    </div>

  </div>
  
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/bootstrap/js/popper.js"></script>
  <script src="<?php echo base_url()?>assets/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/daterangepicker/moment.min.js"></script>
  <script src="<?php echo base_url()?>assets/assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="<?php echo base_url()?>assets/assets/js/main.js"></script>
  <script src="<?php echo base_url()?>assets/sweet-alert/sweetalert.min.js"></script>

</body>
</html>
<script>
  $(document).ready(function(){
    $('.loginform').on('submit',function(e){
        e.preventDefault();
        user = $('#luser').val();
        pass = $('#lpass').val();
        $.ajax({
            url:'<?php echo base_url()?>User/Login/'+user+'/'+pass,
            dataType:'json',
            success:function(data){
                
                if(data.message=="ok"){
                     swal({
                            title: 'Welcome', 
                            text: '', 
                            type: "success",
                            timer: 1200,
                            showConfirmButton: false,
                        },function(){
                            location.reload();
                        })
                }
                else{
                    swal({
                            title: 'Opps', 
                            text: 'Invalid Username/Pasword', 
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false,
                        })
                }
            },
            error:function(){
                swal({
                            title: 'Opps', 
                            text: 'Server Error!', 
                            type: "error",
                            timer: 1200,
                            showConfirmButton: false,
                        })
            }
        });
    });
  });
</script>