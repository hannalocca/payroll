<!doctype html>
<html lang="en">
<head>
    <title>Employee</title>

</head>
<body>

<link rel="stylesheet" href="css/normalize.css">

  
<div class="wrapper">


    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                           <div class="content">

           
                 <form class="form-horizontal label-left employeeregistration" autocomplete="off" style="font-size: 17px;">
                    <div class="row">
                        <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Add Employee Information</h3></div>
                                    <div class="panel-body">
                        <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                            <div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-lg-4 col-xs-12">Select Employee:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <select class="form-control employeeselect" name="employee">
                                                    </select>
                                                </div>
                                            </div><div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-lg-4 col-xs-12">Full name:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <label class="control-label fullnamess">---</label>
                                                </div>
                                            </div><div class="form-group">
                                                <label class="control-label col-md-4 col-sm-4 col-lg-4 col-xs-12">Position:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                     <label class="control-label position">---</label>
                                                </div>
                                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Usertype:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control usertype" name="usertype">
                                        <option value="Staff">Staff</option>
                                        <option value="Admin">Admin</option>
                                    </select>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">User name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control username" name="username">
                                    <small style="color: red;" class="exist">Username already exist!</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Password:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="password" class="form-control password" name="password">
                                    <small style="color: red;" class="passhaha">Password and Re-enter password must be match!</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Re-enter Password:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="password" class="form-control reenter" name="reenter">
                                    <small style="color: red;" class="passhaha">Password and Re-enter password must be match!</small>
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
                        
                            <div class="form-group">
                                 <a href="<?php echo base_url()?>Employee" class="btn-lg btn btn-info pull-right" style="background-color: red;border: red;color: white;">Cancel</a>
                                 <button type="submit" class="btn-lg btn btn-success pull-right saving" style=";background-color:#4267b2;border:1px solid #4267b2;color:white;" data-action="Successfully Saved!" data-actions="save">Save</button>
                            </div>
                       
        </form>
</div>
        </div>   </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
    $(document).ready(function(){
        $('.passhaha').hide();
        $('.exist').hide();
        $('.employeeregistration').bootstrapValidator({
                        fields:{
                            username:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },password:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },reenter:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },employee:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            }
                        }
                     });
        $('.username').on('keyup',function(){
            $.ajax({
                url:'<?php echo base_url()?>User/selectusernameifexist/'+$('.username').val(),
                dataType:'json',
                success:function(data){
                    if(data.message=="ok"){
                        $('.exist').show();
                        $('.saving').attr('disabled',true);
                    }else{
                       $('.exist').hide();
                       $('.saving').removeAttr('disabled');
                    }
                }

            });
        });
        $('.employeeregistration').on('submit',function(e){
            e.preventDefault();
            form = $(this).serialize();
            
            if($('.employeeselect').val()==""||$('.username').val()==""||$('.password').val()==""||$('.reenter').val()==""){
                 $('.employeeregistration').bootstrapValidator('validate');
            }
            else if($('.password').val()!=$('.reenter').val()){
                $('.passhaha').show();
            }
            else{
                swal({
                                  title: "Are you sure to add the User?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Saved!", "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>User/saveUser',
                                            type:'post',
                                            data:form,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>User/Users';
                                            },error:function(data){
                                            alert("Opps. Server Error!"+data);
                                            }
                                            });
                                            });
                                            } else {
                                            swal("Cancelled!", "", "error");
                                            $('.saving').removeAttr('disabled');
                                            }
                                            });
                                            }
                                            });
        
        



        $('.employeeselect').select2({
          placeholder:'<--Select-->'
        });

    loademployee();
    function loademployee(){
        $.ajax({
            url:'<?php echo base_url()?>User/selectEmployeeforAddUsers',
            dataType:'json',
            success:function(data){
                 option = $("<option></option>");
                    option.val("");
                    option.text("SELECT");
                    $('.employeeselect').append(option);
                $.each(data,function(key,val){
                    option = $('<option data-fullname='+val.Lastname+' data-posstion='+val.Position+'></option>');
                    option.val(val.employee_id);
                    option.text(val.fullname);
                    $('.employeeselect').append(option);
                });
            }
        });
    }
    $('.employeeselect').on('change',function(){
        fullname = $(this).find('option:selected').text();
        position = $(this).find('option:selected').data('posstion');
        $('.fullnamess').text(fullname);
        $('.position').text(position);
    });
    });
</script>
