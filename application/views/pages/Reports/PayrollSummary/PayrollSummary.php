<!doctype html>
<html lang="en">
<head>

</head>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.min.css"/>
 
    <script type="text/javascript" src="<?php echo base_url()?>assets/datatable/datatables.min.js"></script>


<body>

<div class="wrapper">
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                            <h2>Payroll Summary</h2>
                            <h3>Select Department and branch</h3>
                            <hr>
                            
                            <div class="col-xs-12 col-md-3 col-sm-3 col-lg-3">
                                <div class="form-group">
                                        <label>
                                        Cut off
                                        </label>
                                        <select class="form-control cut">
                                            <?php foreach($cutoff as $rowss){?>
                                                <option value="<?php echo $rowss->cutoff_id;?>"><?php echo $rowss->from_." ".$rowss->date_from." - ".$rowss->to_." ".$rowss->date_to;?></option>
                                            <?php }?>
                                        </select>
                                </div>
                            </div><div class="col-xs-12 col-md-3 col-sm-3 col-lg-3">
                                <div class="form-group">
                                        <label>
                                        Department
                                        </label>
                                        <select class="form-control dept">
                                            <option value="Corp._Officer">Corp. Officer</option>
                                            <option value="Corp._Employee">Corp. Employee</option>
                                            <option value="Bayad_Center">Bayad Center</option>
                                        </select>
                                </div>
                            </div><div class="col-xs-12 col-md-3 col-sm-3 col-lg-3">
                                <div class="form-group">
                                        <label>
                                        Branch
                                        </label>
                                        <select class="form-control branch">
                                            <?php foreach($branches as $rowss){?>
                                                <option value="<?php echo $rowss->branch_id;?>"><?php echo $rowss->branch_name;?></option>
                                            <?php }?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 col-sm-3 col-lg-3">
                                <div class="form-group">
                                        <button class="btn btn-md btn-success generate">Generate</button>
                                </div>
                            </div>
                            
                            
                            <br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
    $(document).ready(function(){
        $('.generate').on('click',function(){
            cut= $('.cut').val();
            dept= $('.dept').val();
            branch= $('.branch').val();
            window.location.href="<?php echo base_url()?>Payroll/ViewPayrollSummary?cutoff_id="+cut+"&&category="+dept+"&&branch="+branch;
        });
    });
</script>
