<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-size: 11px;">
		<h3><center>JaPhil Global Coins Corporation.</center></h3>
		<h4 class="headerss"><center>Payroll Summary</center></h4>
		<h4 class="cutsum"><center>Cut off:</center></h4>
		<center>
		<table class="table table-bordered append" style="">
			<tr>
				<td style="font-size: 16px;">Fullname</td>
				<td style="font-size: 16px;">Position</td>
				<td style="font-size: 16px;">Salary</td>
				<td style="font-size: 16px;">RatePerDay</td>
				<td style="font-size: 16px;">OT</td>
				<td style="font-size: 16px;">Allowance</td>
				<td style="font-size: 16px;">Adj(s)</td>
				<td style="font-size: 16px;">GrossPay</td>
				<td style="font-size: 16px;">TotalGrossPay</td>
				<td style="font-size: 16px;">Absent</td>
				<td style="font-size: 16px;">UT</td>
				<td style="font-size: 16px;">SSS</td>
				<td style="font-size: 16px;">PHIC</td>
				<td style="font-size: 16px;">HMDF</td>
				<td style="font-size: 16px;">TAX</td>
				<td style="font-size: 16px;">TotalDeductions</td>
				<td style="font-size: 16px;">NetPay</td>
			</tr>
			
		</table>
		</center>
	<input type="hidden" value="<?php echo $_GET['cutoff_id'];?>" id="cutoff_id" name="">
	<input type="hidden" value="<?php echo $_GET['category'];?>" id="cat" name="">
	<input type="hidden" value="<?php echo $_GET['branch'];?>" id="branch" name="">
	<input type="hidden" value="<?php $user = $this->session->userdata('session'); echo $user['Firstname']." ".$user['Middlename']." ".$user['Lastname'];?>" id="user" name="">
</body>
</html>
<script>
	$(document).ready(function(){
		cutoff_id = $('#cutoff_id').val();
		cat = $('#cat').val();
		branch = $('#branch').val();
		accountnumbers();
		function accountnumbers(){
			$.ajax({
				url:'<?php echo base_url()?>Payroll/accountnumbersSummary',
				dataType:'json',
				type:'post',
				data:{cutoff_id:cutoff_id,cat:cat,branch:branch},
				success:function(data){
					var total = 0;
					var totaldeductions = 0;
					var grosspay = 0;
					var tgrosspay = 0;
					var sss = 0;
					var phil = 0;
					var pag = 0;
					var tax = 0;
					var abs = 0;
					var abshour = 0;
					var al = 0;

					$.each(data,function(key,val){
						$('.headerss').text("Payroll Summary for "+val.Department+"(s) - "+val.branch_name+" branch").css({"text-align":"center"});
						tr = '<tr>'
								+'<td>'+val.Lastname+', '+val.Firstname+' '+val.Middlename+' '+val.Suffix+' .</td>'
								+'<td>'+val.Position+'</td>'
								+'<td>'+parseFloat(val.Salary , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.prateperdays , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.povertimeamount , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+' '+val.othour+'h'+val.otmin+'m</td>'
								+'<td>'+parseFloat(val.pallowance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.padjustments, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.pgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.ptotalgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.absentamount , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+' '+val.absentday+'day(s)</td>'
								+'<td>'+parseFloat(val.pundertimeamount , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+' '+val.uthour+'h'+val.utmin+'m</td>'
								+'<td>'+parseFloat(val.psss , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.pphilhealth , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.ppagibig, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.ptax , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.ptotaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
								+'<td>'+parseFloat(val.pnetpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
							+'</tr>';
							total = parseFloat(total) + parseFloat(val.pnetpay);
							totaldeductions = parseFloat(totaldeductions) + parseFloat(val.ptotaldeductions);
							grosspay = parseFloat(grosspay) + parseFloat(val.pgrosspay);
							tgrosspay = parseFloat(tgrosspay) + parseFloat(val.ptotalgrosspay);
							sss = parseFloat(sss) + parseFloat(val.psss);
							phil = parseFloat(phil) + parseFloat(val.pphilhealth);
							pag = parseFloat(pag) + parseFloat(val.ppagibig);
							tax = parseFloat(tax) + parseFloat(val.ptax);
							abs = parseFloat(abs) + parseFloat(val.absentamount);
							abshour = parseFloat(abshour) + parseFloat(val.pundertimeamount);
							al = parseFloat(al) + parseFloat(val.pallowance);
							$('.cutsum').text("Cut-off: "+val.from_+" "+val.date_from+" - "+val.to_+" "+val.date_to+" , "+val.year).css({"text-align":"center","font-weight":"bold"});
							$('.append').append(tr);
					});
					trs = '<tr>'
								+'<td><b>Prepared By:</b></td>'
								+'<td><b>'+$('#user').val()+'</b></td>'
								+'<td></td>'
								+'<td></td>'
								+'<td></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(al, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td><b>Total :</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(grosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(tgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(abs, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(abshour, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(sss, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(phil, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(pag, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(tax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
								+'<td style="text-decoration:underline;"><b>'+parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></td>'
							+'</tr>';
					$('.append').append(trs);
					window.print();
				}
			});
		}
	});
	
</script>