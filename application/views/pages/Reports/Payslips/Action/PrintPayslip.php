<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-size: 13px;">
	<div class="container append">

	</div>
	<input type="hidden" value="<?php echo $_GET['cutoff_id'];?>" id="cutoff_id" name="">
	<input type="hidden" value="<?php echo $_GET['category'];?>" id="cat" name="">
	<input type="hidden" value="<?php echo $_GET['branch'];?>" id="branch" name="">
	<input type="hidden" value="<?php $user = $this->session->userdata('session'); echo $user['Firstname']." ".$user['Middlename']." ".$user['Lastname'];?>" id="user" name="">
</body>
</html>
<script>
	$(document).ready(function(){
		cutoff_id = $('#cutoff_id').val();
		cat = $('#cat').val();
		branch = $('#branch').val();
		selectPayslips();
		function selectPayslips(){
			$.ajax({
				url:'<?php echo base_url()?>Payroll/selectPayrolledfinal',
				dataType:'json',
				type:'post',
				data:{cutoff_id:cutoff_id,cat:cat,branch:branch},
				success:function(data){
					$.each(data,function(key,val){
						tr = '<div class="panel panel-default" style="background-color: #eeeded;">'
                                    +'<div class="panel-heading">Payslip - <b>'+val.Lastname+', '+val.Firstname+' '+val.Middlename+' '+val.Suffix+'</b> Dept.: <b>'+val.Department+'</b> Branch:  <b>'+val.branch_name+'</b> Position : <b>'+val.Position+'</b> <b class="pull-right">Cut-off :'+val.from_+" "+val.date_from+" - "+val.to_+" "+val.date_to+" , "+val.year+'</b></div>'
                                        +'<div class="panel-body">'
                                        	+'<div class="row">'
                                        		+'<h7 style="text-decoration:underline;"><center><b>Japhil Global Coins Corp.</b></h7></center>'
                                        		+'<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left"><b>Gross Pay</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right prateperdays" style="color: white;">.</label>'
														+'</div><div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left"><b style="color: white;">.</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right prateperdays" style="color: white;">.</label>'
														+'</div><div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">Rate per day:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right prateperdays">'+parseFloat(val.prateperdays, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">Number of day(s):</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right pnumberofdays">'+parseFloat(val.pnumberofdays , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left povertimehm">Overtime:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right povertimeamount">'+parseFloat(val.povertimeamount  , 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">Allowance:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right pallowance">'+parseFloat(val.pallowance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">Adjustments:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right padjustments">'+parseFloat(val.padjustments, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"  style="border-bottom:2px solid black;">'
														+'<label class="pull-left"><b>Basic Pay:</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"  style="border-bottom:2px solid black;">'
														+'<label class="pull-right pgrosspay" style="font-weight: bold;">'+parseFloat(val.pgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;">'
														+'<label class="pull-left"><b>Total Gross Pay:</b></label>'
														+'</div>'
														+'<hr>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;">'
														+'<label class="pull-right ptotalgrosspay" style="font-weight: bold;">'+parseFloat(val.ptotalgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
                                        		+'</div>'


                                        		+'<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left"><b>Less:Deductions</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right" style="color: #fff;">_____________</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left pabsent">Absent:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right absentamount">'+parseFloat(val.absentamount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left pundertimehm">Undertime:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right pundertimeamount">'+parseFloat(val.pundertimeamount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">Advances:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right pcashadvance">'+parseFloat(val.pcashadvance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">SSS:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right psss">'+parseFloat(val.psss, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">PHIC:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right pphilhealth">'+parseFloat(val.pphilhealth, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">HDMF:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right ppagibig">'+parseFloat(val.ppagibig, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div><div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-left">TAX:</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">'
														+'<label class="pull-right ptax">'+parseFloat(val.ptax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;border-top:2px solid black;">'
														+'<label class="pull-left" style="color: red;"><b>Total Deductions:</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;border-top:2px solid black;">'
														+'<label class="pull-right ptotaldeductions" style="color:red;"><b>'+parseFloat(val.ptotaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6">'
														+'<label class="pull-left"><b>NET PAY:</b></label>'
														+'</div>'
														+'<div class="col-md-6 col-sm-6 col-lg-6">'
														+'<label class="pull-right pnetpay" style="font-weight: bold;text-decoration: underline;">'+parseFloat(val.pnetpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</label>'
														+'</div>'
                                        		+'</div>'
                                        	+'</div>'
                                            
		                                +'</div>'
		                            +'<label>Prepared By:'+$('#user').val()+'</label></div>';
		                            $('.append').append(tr);
					});
window.print();
				}
			});
		}
	});
	
</script>