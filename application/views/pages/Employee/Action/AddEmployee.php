<!doctype html>
<html lang="en">
<head>
    <title>Employee</title>

</head>
<body>

<link rel="stylesheet" href="css/normalize.css">

  
    <style>
        .autoCaps{
            text-transform: capitalize;
        }
    </style>
<div class="wrapper">


    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                           <div class="content">

           
        <?php if(isset($_GET['emp_id'])){?>
             <form class="form-horizontal label-left employeeregistration" autocomplete="off" style="font-size: 17px;">
                <?php foreach($info as $row){?>
                <input type="hidden" name="emp_id" value="<?php echo $_GET['emp_id']; ?>">
                    <div class="row">
                        <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Update Employee Information</h3></div>
                                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                            
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">First name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control fname autoCaps" value="<?php echo $row->Firstname;?>" name="fname">
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Middle name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control middle autoCaps" value="<?php echo $row->Middlename;?>" name="mname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Last name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control lasts autoCaps" value="<?php echo $row->Lastname;?>" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Suffix:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control autoCaps" value="<?php echo $row->Suffix;?>" name="suffix">
                                    <small>(Optional)</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Gender:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                   <select class="form-control" name="gender">
                                    <?php if($row->Gender=="Male"){?>
                                        <option value='Male' selected="true">Male</option>
                                        <option value='Female'>Female</option>
                                   <?php }else{?>
                                        <option value='Male'>Male</option>
                                        <option value='Female' selected="true">Female</option>
                                   <?php }?>
                                        
                                    </select>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Birth date:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="date" class="form-control birth" value="<?php echo $row->Birthdate;?>" name="birth">
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                             
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Contact:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control contact" value="<?php echo $row->Contact;?>" name="contact">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Address:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control address autoCaps" value="<?php echo $row->Address;?>" name="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Zip Code:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control zip" value="<?php echo $row->Zip_code;?>" name="zip">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Marital Status:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control" name="mstatus">
                                        
                                        <?php if($row->Marital_status=="Single"){?>
                                            <option value="Single" selected="true">Single</option>
                                            <option value="Married">Married</option>
                                            <option value="Divorced">Divorced</option>
                                            <option value="Widowed">Widowed</option>
                                        <?php }else if($row->Marital_status=="Married"){?>
                                            <option value="Single">Single</option>
                                            <option value="Married" selected="true">Married</option>
                                            <option value="Divorced">Divorced</option>
                                            <option value="Widowed">Widowed</option>
                                        <?php }else if($row->Marital_status=="Divorced"){?>
                                            <option value="Single">Single</option>
                                            <option value="Married">Married</option>
                                            <option value="Divorced" selected="true">Divorced</option>
                                            <option value="Widowed">Widowed</option>
                                        <?php }else if($row->Marital_status=="Widowed"){?>
                                            <option value="Single">Single</option>
                                            <option value="Married">Married</option>
                                            <option value="Divorced">Divorced</option>
                                            <option value="Widowed"  selected="true">Widowed</option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Identification Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" value="<?php echo $row->Identification_number;?>" name="id">
                                    <small>(Optional)</small>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Department:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control forbranch" name="department">
                                        
                                        <?php if($row->Department=="Corp._Officer"){?>
                                            <option value="Corp._Officer" selected="true">Corp. Officer</option>
                                            <option value="Corp._Employee">Corp. Employee</option>
                                            <option value="Bayad_Center">Bayad Center</option>
                                        <?php }else if($row->Department=="Corp._Employee"){?>
                                            <option value="Corp._Officer">Corp. Officer</option>
                                            <option value="Corp._Employee" selected="true">Corp. Employee</option>
                                            <option value="Bayad_Center">Bayad Center</option>
                                        <?php }else if($row->Department=="Bayad_Center"){?>
                                            <option value="Corp._Officer">Corp. Officer</option>
                                            <option value="Corp._Employee" >Corp. Employee</option>
                                            <option value="Bayad_Center" selected="true">Bayad Center</option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group branches">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Branch:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control branch" name="branch">
                                        <?php foreach($branches as $rowss){?>
                                            <option value="<?php echo $rowss->branch_id;?>"><?php echo $rowss->branch_name;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Position:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control position autoCaps" value="<?php echo $row->Position;?>" name="position">
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Salary:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control salary" value="<?php echo $row->Salary;?>" name="salary">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Rate Per Day:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control rate" value="<?php echo $row->Rate_perday;?>" name="rate">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Monthly Allowance:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control allowance" value="<?php echo $row->Allowance;?>" name="allowance">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Bank Account Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control bankacc" value="<?php echo $row->Bank_account_number;?>" name="bankacc">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Hired date:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="date" class="form-control hired" value="<?php echo $row->Hired_date;?>" name="hired">
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
                        
                    

                <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Deductions</h3></div>
                                    <div class="panel-body">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">SSS Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control sssnumber" value="<?php echo $row->SSS_number;?>" name="sssnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PHILHEALTH Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control philnumber" value="<?php echo $row->PHILHEALTH_number;?>" name="philnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PAG-IBIG Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control pagnumber" value="<?php echo $row->PAGIBIG_number;?>" name="pagnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">TIN Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control tinumber" value="<?php echo $row->TIN_number;?>" name="taxnum">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">SSS Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control sssamount" value="<?php echo $row->SSS_amount;?>" name="sssamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PHILHEALTH Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control philamount" value="<?php echo $row->PHILHEALTH_amount;?>" name="philamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PAG-IBIG Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control pagamount" value="<?php echo $row->PAGIBIG_amount;?>" name="pagamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">TAX Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control taxamount" value="<?php echo $row->TAX;?>" name="taxamount">
                                </div>
                            </div>
                        </div></div>
                            </div>
                            <div class="form-group">
                                 <a href="<?php echo base_url()?>Employee" class="btn-lg btn btn-info pull-right" style=";background-color:red;border:1px solid red;color:white;">Cancel</a>
                                 <button type="submit" class="btn-lg btn btn-success pull-right saving" style=";background-color:#4267b2;border:1px solid #4267b2;color:white;" data-action="Successfully Updated!" data-actions="update">Update</button>
                            </div>
                       <?php }?>
        </form>
            <?php }else{?>
                 <form class="form-horizontal label-left employeeregistration" autocomplete="off" style="font-size: 17px;">
                    <div class="row">
                        <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Add Employee Information</h3></div>
                                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">First name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control fname autoCaps" name="fname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Middle name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control middle autoCaps" name="mname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Last name:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control lasts autoCaps" name="lname">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Suffix:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control autoCaps" name="suffix">
                                    <small>(Optional)</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Gender:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                   <select class="form-control" name="gender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Birth date:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="date" class="form-control birth" name="birth">
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                             
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Contact:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control contact" name="contact">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Address:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control address autoCaps" name="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Zip Code:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control zip" name="zip">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Marital Status:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control" name="mstatus">
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Widowed">Widowed</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Identification Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" name="id">
                                    <small>(Optional)</small>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Department:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control forbranch" name="department">
                                        <option value="Corp._Officer">Corp. Officer</option>
                                        <option value="Corp._Employee">Corp. Employee</option>
                                        <option value="Bayad_Center">Bayad Center</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group branches">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Branch:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <select class="form-control branch" name="branch">
                                        <?php foreach($branches as $rowss){?>
                                            <option value="<?php echo $rowss->branch_id;?>"><?php echo $rowss->branch_name;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Position:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control position autoCaps" name="position">
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-xs-12 col-md-4 col-sm-4 col-lg-4">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Salary:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control salary" name="salary">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Rate Per Day:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control rate" name="rate">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Monthly Allowance:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control allowance" name="allowance">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Bank Account Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control bankacc" name="bankacc">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Hired date:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="date" class="form-control hired" name="hired">
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
                        
                    

                <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Deductions</h3></div>
                                    <div class="panel-body">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">SSS Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control sssnumber" name="sssnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PHILHEALTH Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control philnumber" name="philnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PAG-IBIG Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control pagnumber" name="pagnum">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">TIN Number:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control tinumber" name="taxnum">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">SSS Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control sssamount" name="sssamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PHILHEALTH Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control philamount" name="philamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">PAG-IBIG Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control pagamount" name="pagamount">
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">TAX Amount:</label>
                                <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control taxamount" name="taxamount">
                                </div>
                            </div>
                        </div></div>
                            </div>
                            <div class="form-group">
                                 <a href="<?php echo base_url()?>Employee" class="btn-lg btn btn-info pull-right" style="background-color: red;border: red;color: white;">Cancel</a>
                                 <button type="submit" class="btn-lg btn btn-success pull-right saving" style=";background-color:#4267b2;border:1px solid #4267b2;color:white;" data-action="Successfully Saved!" data-actions="save">Save</button>
                            </div>
                       
        </form>
            <?php }?>
</div>
        </div>   </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
/*$(document).ready(function () {
    $('#example').DataTable();

});*/
</script>

            <script>
                $(document).ready(function(){
                   
                    $('.forbranch').trigger('change');
                    $('.salary').on('keyup',function(){
                        val = parseFloat($(this).val().replace(',',''));
                        rate = val / 22;
                        $('.rate').val(rate.toFixed(2));


                    });
                    $('.rate').on('keyup',function(){
                        val = parseFloat($(this).val().replace(',',''));
                        rate = val * 22;
                        $('.salary').val(rate.toFixed(2));


                    });
                    $('.rate').maskMoney();
                    $('.salary').maskMoney();
                    $('.allowance').maskMoney();
                    $('.sssamount').maskMoney();
                    $('.philamount').maskMoney();
                    $('.pagamount').maskMoney();
                    $('.taxamount').maskMoney();

                    $('.rate').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','rate');
                    });$('.salary').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','salary');
                    });$('.allowance').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','allowance');
                    });$('.sssamount').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','sssamount');
                    });$('.philamount').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','philamount');
                    });$('.pagamount').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','pagamount');
                    });$('.taxamount').on('keyup',function(){
                        $('.employeeregistration').bootstrapValidator('revalidateField','taxamount');
                    });
                    $('.employeeregistration').bootstrapValidator({
                        fields:{
                            fname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },birth:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },contact:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },mname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },address:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },position:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },lname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },fname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },zip:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },hired:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            }
                        }
                     });
                     $('.employeeregistration').on('submit',function(e){
                        dataaction = $('.saving').data('action');
                        dataactions = $('.saving').data('actions');
                      e.preventDefault();
                      form = $(this).serialize();
                      if($('.fname').val()==""||$('.birth').val()==""||$('.contact').val()==""||$('.middle').val()==""||$('.address').val()==""||$('.position').val()==""||$('.lasts').val()==""||$('.zip').val()==""||$('.hired').val()==""){
                        $('.employeeregistration').bootstrapValidator('validate');
                      }
                      else{
                            swal({
                                  title: "Are you sure to "+dataactions+" this Employee Informations?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal(dataaction, "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>Employee/saveEmployee',
                                            dataType:'json',
                                            type:'post',
                                            data:form,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>Employee';
                                            }
                                        }); 
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
                      }
                    
                    
                    });
                });
                              
            </script>

