<!doctype html>
<html lang="en">
<head>
    <title>Employee</title>

</head>
<body>

<link rel="stylesheet" href="css/normalize.css">

  <div class="modal fade" id="confirm" style="height: 600px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nofitications</h5>
      </div>
      <div class="modal-body" style="height: 300px;overflow: auto;">
          <center><h5 style="color:green;"><b>New</b></h5></center>
          <table class="table table-bordered notitable"> 
              
          </table>
          <center><h5><b>Old</b></h5></center>
          <table class="table table-bordered seennotitable"> 
              
          </table>
      </div>
        
      <div class="modal-header" style="height: 300px;overflow: auto;">
          <button type="button" class="btn btn-info btn-lg pull-right" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-info btn-lg pull-right yes" data-dismiss="modal">Yes</button>
      </div>
      
    </div>
  </div>
</div>
    <style>
        .autoCaps{
            text-transform: capitalize;
        }
    </style>
<div class="wrapper">


    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: gray;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                           <div class="content">

           
        <?php if(isset($_GET['emp_id'])){?>
                         <form class="form-horizontal label-left employeeregistration" autocomplete="off">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <?php foreach($info as $row){?>
                <input type="hidden" name="emp_id" value="<?php echo $_GET['emp_id']; ?>">
                    <div class="row">
                        <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Employee Information View</h3></div>
                                    <div class="panel-body">
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">First name:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Firstname;?></label>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Middle name:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Middlename;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Last name:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Lastname;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Suffix:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                     <label class="control-label"> <?php echo $row->Suffix;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Gender:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Gender;?></label>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Birth date:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Birthdate;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Contact:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Contact;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Address:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Address;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Bank Account Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Bank_account_number;?></label>
                                </div>
                            </div>
                            
                            
                        </div>

                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Zip Code:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Zip_code;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Marital Status:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Marital_status;?></label>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Identification Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Identification_number;?></label>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Department:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Department;?></label>

                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Branch:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->branch_name;?></label>

                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Position:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Position;?></label>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Rate Per Day:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Rate_perday;?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Salary:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Salary;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Monthly Allowance:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Allowance;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">Hired date:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                    <label class="control-label"> <?php echo $row->Hired_date;?></label>
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading"><h3>Deductions</h3></div>
                                    <div class="panel-body">
                    <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md64 col-sm-6 col-lg-6">SSS Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                     <label class="control-label"> <?php echo $row->SSS_number;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">PHILHEALTH Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                     <label class="control-label"> <?php echo $row->PHILHEALTH_number;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">PAG-IBIG Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                     <label class="control-label"> <?php echo $row->PAGIBIG_number;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">TIN Number:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                     <label class="control-label"> <?php echo $row->TIN_number;?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">SSS Amount:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                                     <label class="control-label"> <?php echo $row->SSS_amount;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">PHILHEALTH Amount:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                                     <label class="control-label"> <?php echo $row->PHILHEALTH_amount;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">PAG-IBIG Amount:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                                     <label class="control-label"> <?php echo $row->PAGIBIG_amount;?></label>
                                </div>
                            </div><div class="form-group">
                                <label class="control-label col-xs-12 col-md-6 col-sm-6 col-lg-6">TAX Amount:</label>
                                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">

                                     <label class="control-label"> <?php echo $row->TAX;?></label>
                                </div>
                            </div>
                        </div></div>
                            </div>
                            <div class="form-group">
                                 <a href="<?php echo base_url()?>Employee" class="btn-lg btn btn-info pull-right" >Back</a>
                            </div>
                       <?php }?>
            </div>

                
                        
                    

                
        </form>
            <?php }?>
</div>
        </div>   </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    $('#example').DataTable();

});
</script>

            <script>
                $(document).ready(function(){
                    $('.employeeregistration').bootstrapValidator({
                        fields:{
                            fname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },birth:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },contact:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },mname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },address:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },position:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },rate:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },lname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },fname:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },zip:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },salary:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },sssnum:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },philnum:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },pagnum:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },sssamount:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },philamount:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },pagamount:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },taxamount:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },taxnum:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            },hired:{
                                validators:{
                                    notEmpty:{
                                        message:'This field is required'
                                    }
                                }
                            }
                        }
                     });
                     $('.employeeregistration').on('submit',function(e){
                      e.preventDefault();
                      form = $(this).serialize();
                      if($('.fname').val()==""||$('.birth').val()==""||$('.contact').val()==""||$('.middle').val()==""||$('.address').val()==""||$('.position').val()==""||$('.rate').val()==""||$('.lasts').val()==""||$('.zip').val()==""||$('.salary').val()==""||$('.sssnumber').val()==""||$('.philnumber').val()==""||$('.pagnumber').val()==""||$('.sssamount').val()==""||$('.philamount').val()==""||$('.pagamount').val()==""||$('.taxamount').val()==""||$('.tinumber').val()==""||$('.hired').val()==""){
                        $('.employeeregistration').bootstrapValidator('validate');
                      }
                      else{
                            swal({
                                  title: "Are you sure to save this Employee Informations?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Saved!", "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>Employee/saveEmployee',
                                            dataType:'json',
                                            type:'post',
                                            data:form,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>Employee';
                                            }
                                        }); 
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
                      }
                    
                    
                    });
                });
                              
            </script>

