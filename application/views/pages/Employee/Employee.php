<!doctype html>
<html lang="en">
<head>
    <title>Employee Information</title>

</head>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.min.css"/>
 
    <script type="text/javascript" src="<?php echo base_url()?>assets/datatable/datatables.min.js"></script>
<body>
<div class="modal fade" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background-color: #eeeded;">
        <h5 class="modal-title"><h3>Are you sure to change the status? If you are sure. Please, change it.</h3></h5>
      </div>
        <form class="form-horizaontal label-left changestatusform">
                  <div class="modal-body" style="height: 300px;background-color: #eeeded;">
                        <input type="hidden" class="cid">
                        <div class="form-group">
                                            <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Employee name:</label>
                                            <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                                <label class="control-label fullname"></label>
                                            </div>
                                        
                       
                    </div>
                                     <div class="form-group">
                                            <label class="control-label col-xs-12 col-md-4 col-sm-4 col-lg-4">Employee Status:</label>
                                            <div class="col-xs-12 col-md-8 col-sm-8 col-lg-8">
                                                <select class="form-control cstatss">
                                                    <option value="Employee">Employee</option>
                                                    <option value="Resigned">Resigned</option>
                                                    <option value="Terminated">Terminated</option>
                                                    <option value="End_of_contract">End of contract</option>
                                                    <option value="Retired">Retired</option>
                                                    <option value="AWOL">AWOL</option>
                                                </select>
                                            </div>
                                            <br>
                                            <br>
                    </div>
                    <div class="modal-footer"  style="background-color: #eeeded;">
                                <button type="button" class="btn btn-info btn-md pull-right" style="background-color: red;border: red;color: white;" data-dismiss="modal">Cancel</button>
                                <button type="submit" style="background-color: #4267b2;border: #4267b2;color: white;" class="btn btn-info btn-md pull-right yes">Save</button>
                    </div>
                </form>

      </div>
    </div>
  </div>
</div>
<div class="wrapper">


    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                            <h3>List of Employee(s) Information</h3>
                            <a data-toggle="tooltip" title="Add Employee" href="<?php echo base_url()?>AddEmployee" class="pull-right btn btn-md btn-info" style=";background-color:red;border:1px solid red;color:white;"><i class="fa fa-user-plus"></i></a>
                            <hr>
                            <br>
                            <table id="example" class="display">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Firstname</th>
                                        <th>Middlename</th>
                                        <th>Lastname</th>
                                        <th>Suffix</th>
                                        <th>Department</th>
                                        <th>Position</th>
                                        <th>Salary</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($emp as $row){?>
                                    <tr>
                                        <td><?php echo $row->Employee_id;?></td>
                                        <td><?php echo $row->Firstname;?></td>
                                        <td><?php echo $row->Middlename;?></td>
                                        <td><?php echo $row->Lastname;?></td>
                                         <td><?php echo $row->Suffix;?></td>
                                         <td><?php echo $row->Department;?></td>
                                        <td><?php echo $row->Position;?></td>
                                        <td><?php echo $row->Salary;?></td>
                                        <td><?php if($row->Status=="End_of_contract"){echo "End of contract"; }else{echo $row->Status;}?></td>
                                        <td>
                                        <a data-toggle="tooltip" title="View" style="background-color: #1DC7EA;color:white;" href="<?php echo base_url()?>Employee/ViewEmployee?emp_id=<?php echo $row->Employee_id;?>" class="btn btn-info btn-sm" tooltip="View"><i class="fa fa-eye"></i></a>
                                        <a data-toggle="tooltip" title="Update" style="background-color: #87CB16;color:white;" href="<?php echo base_url()?>Employee/UpdateEmployee?emp_id=<?php echo $row->Employee_id;?>" class="btn btn-success btn-sm"><i class="fa fa-folder-open"></i></a>
                                        <a data-toggle="modal" data-toggle="tooltip" title="Change Status" style="background-color: #888888;color:white;" href="#" class="btn btn-default btn-sm changestatus" data-fullname="<?php echo $row->Firstname." ".$row->Middlename." ".$row->Lastname." ".$row->Suffix;?>" data-id="<?php echo $row->Employee_id;?>" data-toggle="modal" data-target="#confirm"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="modal" data-toggle="tooltip" title="Delete" style="background-color: #FF4A55;color:white;" href="#" class="btn btn-danger btn-sm delete" data-fullname="<?php echo $row->Firstname." ".$row->Middlename." ".$row->Lastname." ".$row->Suffix;;?>" data-id="<?php echo $row->Employee_id;?>"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    $('#example').DataTable();
    $('.changestatus').on('click',function(){
        id = $(this).data('id');
        name = $(this).data('fullname');
        $('.cid').val(id);
        $('.fullname').text(name);
    });
    $('.changestatusform').on('submit',function(e){
        e.preventDefault();
        id = $('.cid').val();
        cstatss = $('.cstatss').val();
        $.ajax({
            url:'<?php echo base_url()?>Employee/changeEmpStatus/'+id+'/'+cstatss,
            success:function(){
                 swal({
                            title: 'Status Changed!', 
                            text: '', 
                            type: "success",
                            timer: 1200,
                            showConfirmButton: false,
                        },function(){
                            location.reload();
                        })
            },error:function(){
                swal({
                            title: 'Server Error!', 
                            text: '', 
                            type: "error",
                            timer: 1200,
                            showConfirmButton: false,
                        });
            }
        });
    });
    $(document).on('click','.delete',function(){
        name = $(this).data('fullname');
        id = $(this).data('id');
        swal({
                                  title: "Are you sure to delete "+name+"?",
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Deleted!", "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>Employee/deletes/'+id,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>Employee';
                                            }
                                        }); 
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
    });
});
</script>
