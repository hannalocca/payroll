<!doctype html>
<html lang="en">
<head>

</head>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.min.css"/>
 
    <script type="text/javascript" src="<?php echo base_url()?>assets/datatable/datatables.min.js"></script>
<div class="modal fade" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background-color: #eeeded;">
        <h5 class="modal-title"><h3>Add Branch</h3></h5>
      </div>
      <form class="form-horizontal label-left addbranchform" autocomplete="off" style="font-size: 17px;">
            <div class="modal-body"  style="background-color: #eeeded;">
                <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">

                    <div class="form-group">
                        <label class="control-label">Branch name:</label>
                        <input type="text" required placeholder="Enter Branch name" class="form-control bbranchname" name="">
                    </div>
                </div>
            </div> 
           <div class="modal-footer"  style="background-color: #eeeded;">
                <button type="button" class="btn btn-info btn-md pull-right" style="background-color: red;border: red;color: white;" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-info btn-md pull-right" style="background-color: #4267b2;border: #4267b2;color: white;">Save</button>
            </div>   
        </form> 
      </div>
    </div>
  </div>

<body>

<div class="wrapper">
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                            <h3>List of Branch(es)</h3>
                            <a data-toggle="modal" data-toggle="tooltip" title="Create Cut-off" href="#" data-toggle="modal" data-target="#confirm" class="pull-right btn btn-md btn-info" style=";background-color:red;border:1px solid red;color:white;"><i class="fa fa-user-plus"></i></a>

                            <hr>
                            <br>
                            <table id="example" class="display">
                                <thead>
                                    <tr>
                                        <th>Branch name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($branches as $row){?>
                                        <tr>
                                            <td><?php echo $row->branch_name;?></td>
                                        </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    $('#example').DataTable();
    
    
    $('.addbranchform').on('submit',function(e){
        e.preventDefault();
        branch = $('.bbranchname').val();
        swal({
                                  title: "Are you sure to add this Branch?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                $.ajax({
                                            url:'<?php echo base_url()?>Settings/addBranch/'+branch,
                                            success:function(data){
                                                swal("Saved!", "", "success"); 
                                            },
                                            error:function(){
                                                swal({
                                                    title: 'Opps', 
                                                    text: 'Server Error!', 
                                                    type: "error",
                                                    timer: 1200,
                                                    showConfirmButton: false,
                                                })
                                            }
                                        }); 
                                
                                $('.confirm').on('click', function(e){
                                    window.location.href='<?php echo base_url()?>Settings/Branches';
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
        
    });
});
</script>
