<!doctype html>
<html lang="en">
<head>

</head>
<body>

<div class="wrapper">
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row">
                            <div class="timeline-left" >
                            <h3 style="color:red;">Payroll</h3>
                            <hr>
                            <div class="col-md-7 col-sm-7 col-lg-7 col-xs-12">
                                <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading">Input</div>
                                    <div class="panel-body">
                                        <h3 style="color:red;">Gross Pay</h3>
                                        <form class="form-horizontal form-label-right formcompute">
                                            <div class="col-md-7 col-sm-7 col-lg-7 col-xs-12">
                                                <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Select Employee:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <select class="form-control employeeselect" name="employee">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Payroll Period:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <select class="form-control payrollperiod">
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Daily">Daily</option>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="form-group monthlyperiod">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Basic Pay:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <label class="control-label basicpay">---</label>
                                                </div>
                                            </div><div class="form-group noofdays">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Number of days:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <input type="number" class="form-control" id="noofdaysfromfrom" value="0" name="nodays">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Allowance:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <label class="control-label allowance">---</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Add Overtime<input type="checkbox" id="chkovertime" name=""></label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 overtime">
                                                    <input type="number" placeholder="Number of hours" value="0" class="form-control otnoofhours iovertimehour" name="iovertimehour"><b>hour(s)</b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12"></label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 overtime">
                                                    <input type="number" class="form-control iovertimeminute" value="0" name="iovertimeminute" placeholder="Input minute(s)"><b>minute(s)</b>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Add Adjustments<input type="checkbox" id="adjustments" name=""></label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <input type="number" placeholder="Enter amount" value="0" class="form-control adjustments" name="adjustments"><b class="adjustments">amount(s)</b>
                                                </div>
                                            </div>
                                            <h3 style="color:red;">Deductions</h3>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Absent:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <input type="number" class="form-control iabsent" value="0" name="absent" placeholder="Input day(s)"><b>day(s)</b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Undertime:<input type="checkbox" id="chkundertime" name=""></label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 undertime">
                                                    <input type="number" class="form-control iundertimehour" value="0" name="undertimehour" placeholder="Input hour(s)"><b>hour(s)</b>
                                                    
                                                </div>
                                            </div><div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12"></label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12 undertime">
                                                    <input type="number" class="form-control iundertimeminute" value="0" name="undertimeminute" placeholder="Input minute(s)"><b>minute(s)</b>
                                                    
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Cash Advance:</label>
                                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <input type="text" class="form-control icashadvance" value="0" id="cashadvancefromform" placeholder="Input cash" name="cashadvance"><b>amount</b>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group">
                                                   <button class="btn btn-sm btn-info pull-right" style="background-color: red;color:white;border:0;" type="submit">Compute</button>
                                                
                                            </div>
                                        </form>     
                                            
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-lg-5 col-xs-12">
                                                <div class="form-group" style="border:1px solid #eaeaea;padding: 3px;">
                                                    <input type="hidden" value="800.00" id="ratefromform" name="">
                                                    <input type="hidden" value="" id="perdayfromform" name="">
                                                    <input type="hidden" value="" id="minutesforundertime" name="">
                                                    <input type="hidden" value="" id="hourforundertime" name="">
                                                    <input type="hidden" value="" id="taxjud" name="">
                                                    <input type="hidden" value="" id="sssjud" name="">
                                                    <input type="hidden" value="" id="philjud" name="">
                                                    <input type="hidden" value="" id="pagjud" name="">
                                                    <input type="hidden" value="<?php echo $_GET['cutoff_id'];?>" id="cutoff_id" name="">
                                                    <input type="hidden" value="<?php echo $_GET['range'];?>" id="rangejus" name="">
                                                    <p><b>Employee:</b></p>
                                                    <p class="pfullname control-label"><b>Fullname: ---</b></p>
                                                    <p class="pdepartment control-label"><b>Department: ---</b></p>
                                                    <p class="pposition control-label"><b>Position: ---</b></p>
                                                    <p class="prateperday control-label"><b>Rate per day: ---</b></p>
                                                    <p class="daccountnumber control-label"><b>Bank Account Number: ---</b></p>
                                                    
                                                </div>
                                            </div>
                                        
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-lg-5 col-xs-12">
                                <div class="panel panel-default" style="background-color: #eeeded;">
                                    <div class="panel-heading">Payslip</div>
                                        <div class="panel-body">
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">Rate per day:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right prateperdays">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">Number of day(s):</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right pnumberofdays">0</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left povertimehm">Overtime:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right povertimeamount">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">Allowance:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right pallowance">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">Adjustments:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right padjustments">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"  style="border-bottom:2px solid black;">
                                                <p class="pull-left"><b>Basic Pay:</b></p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12"  style="border-bottom:2px solid black;">
                                                <p class="pull-right pgrosspay" style="font-weight: bold;">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;">
                                                <p class="pull-left">Total Gross Pay:</p>
                                            </div>
                                            <hr>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;">
                                                <p class="pull-right ptotalgrosspay" style="font-weight: bold;">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left"><b>Less:Deductions</b></p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right" style="color: #fff;">_____________</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left pabsent">Absent:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right absentamount">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left pundertimehm">Undertime:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right pundertimeamount">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">Advances:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right pcashadvance">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">SSS:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right psss">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">PHIC:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right pphilhealth">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">HDMF:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right ppagibig">0.00</p>
                                            </div><div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-left">TAX:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                <p class="pull-right ptax">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;border-top:2px solid black;">
                                                <p class="pull-left">Total Deductions:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" style="border-bottom:2px solid black;border-top:2px solid black;">
                                                <p class="pull-right ptotaldeductions" style="color:red;">0.00</p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6">
                                                <p class="pull-left"><b>NET PAY:</b></p>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-lg-6">
                                                <p class="pull-right pnetpay" style="font-weight: bold;">0.00</p>
                                            </div>
                                            <button class="btn btn-sm btn-info btnsavess savenajudangpayslip" style="background-color: #4267b2;border: 0;color: white;">Save</button>
                                            <button class="btn btn-sm btn-info computeagain btnsavess" style="background-color: red;border: 0;color: white;">Compute Again</button>
                                        </div>
                                </div>
                            </div>
                            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 col-xs-12 col-lg-1 col-sm-1">
                            
                        </div><div class="col-md-10 col-xs-12 col-lg-10 col-sm-10">
                            <h3 style="color:red;"><center>Payslips<center></h3>
                                <!-- <a data-toggle="tooltip" title="Print Account Number/Overall Total Amount Summary" href="<?php echo base_url()?>Payroll/PrintAccountNumbers?cutoff_id=<?php echo $_GET['cutoff_id'];?>" class="btn btn-success btn-md pull-right" style="background-color: #4267b2;border: 0;color: white;"><i class="fa fa-print"></i></a> -->
                               <!--  <a data-toggle="tooltip" title="Print all Bayad Center(s) payslips" href="<?php echo base_url()?>Payroll/PrintPayslip?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Bayad_Center" class="btn btn-success btn-md pull-left" style="background-color: red;border: 0;color: white;"><i class="fa fa-print"></i> </a>
                                <a data-toggle="tooltip" title="Print all Corporate Officer(s) payslips" href="<?php echo base_url()?>Payroll/PrintPayslip?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Corp._Officer" class="btn btn-success btn-md pull-left" style="background-color: #4267b2;border: 0;color: white;"><i class="fa fa-print"></i></a>
                                <a data-toggle="tooltip" title="Print all Corporate Employee(s) payslips" href="<?php echo base_url()?>Payroll/PrintPayslip?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Corp._Employee" class="btn btn-success btn-md pull-left" style="background-color: gray;border: 0 ;color: white;"><i class="fa fa-print"></i></a> -->
<!-- 
                               <a data-toggle="tooltip" title="Print all payslips" href="<?php echo base_url()?>Payroll/PrintPayslip?cutoff_id=<?php echo $_GET['cutoff_id'];?>" class="btn btn-success btn-md pull-right" style="background-color: red;border: 0;color: white;"><i class="fa fa-print"></i> </a> 
                                <a data-toggle="tooltip" title="Print Account Number/Overall Total Amount Summary" href="<?php echo base_url()?>Payroll/PrintAccountNumbers?cutoff_id=<?php echo $_GET['cutoff_id'];?>" class="btn btn-success btn-md pull-right" style="background-color: #4267b2;border: 0;color: white;"><i class="fa fa-print"></i></a>
                                <a data-toggle="tooltip" title="Print Corp._Officer Payroll Summary" href="<?php echo base_url()?>Payroll/PayrollSummary?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Corp._Officer" class="btn btn-success btn-md pull-right" style="background-color: gray;border: 0 ;color: white;"><i class="fa fa-print"></i></a>
                                <a data-toggle="tooltip" title="Print Corp.Employee Payroll Summary" href="<?php echo base_url()?>Payroll/PayrollSummary?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Corp._Employee" class="btn btn-success btn-md pull-right" style="background-color: gray;border: 0 ;color: white;"><i class="fa fa-print"></i></a>
                                <a  title="Print Bayad Center Payroll Summary" data-toggle="tooltip"  href="<?php echo base_url()?>Payroll/PayrollSummary?cutoff_id=<?php echo $_GET['cutoff_id'];?>&&category=Bayad_Center" class="btn btn-success btn-md pull-right" style="background-color: gray;border: 0 ;color: white;"><i class="fa fa-print"></i></a> -->
                            <table class="table table-bordered tblpayrolled" style="background-color: #eeeded;">
                                <tr>
                                    <th>Firstname</th>
                                    <th>Middle</th>
                                    <th>Lastname</th>
                                    <th>Department</th>
                                    <th>Position</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    
                                </tr>
                            </table>
                        </div><div class="col-md-1 col-xs-12 col-lg-1 col-sm-1">
                           
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    selectPayrolled();
    function selectPayrolled(){
        cutoff_id = $('#cutoff_id').val();
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectPayrolled',
            data:{cutoff_id:cutoff_id},
            dataType:'json',
            type:'post',
            success:function(data){
                $.each(data,function(key,val){
                    tr = '<tr><td>'+val.Firstname+'</td>'
                        +'<td>'+val.Middlename+'</td>'
                        +'<td>'+val.Lastname+'</td>'
                        +'<td>'+val.Department+'</td>'
                        +'<td>'+val.Position+'</td>'
                        +'<td><a data-toggle="tooltip" title="Print" href="<?php echo base_url()?>Payroll/PrintPayslipSpecific?payroll_id='+val.payroll_id+'" class="btn btn-md btn-success" style="background-color:#87CB16;border:0;color:white;"><i class="fa fa-print"></i> </a><button data-toggle="tooltip" title="Delete" data-id="'+val.payroll_id+'" style="background-color:red;color:white;border:0;" type="button" class="btn btn-md btn-danger deletenapayroll" data-fullname="'+val.Lastname+', '+val.Firstname+' '+val.Middlename+' '+val.Suffix+' ."><i class="fa fa-trash"></i> </button></td></tr>';
                        $('.tblpayrolled').append(tr);
                });
            },error:function(){
                alert("Opps! Server Error.");
            }
        });
    }
    $('.btnsavess').hide();
    $('.formcompute').bootstrapValidator({
        fields:{
            employee:{
                validators:{
                    notEmpty:{
                        message:'This field is required.'
                    }
                }
            },undertimehour:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Under time).'
                    }
                }
            },undertimeminute:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Under time).'
                    }
                }
            },iovertimehour:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Over time).'
                    }
                }
            },iovertimeminute:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Over time).'
                    }
                }
            },cashadvance:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Cash Advance)'
                    }
                }
            },absent:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Absent).'
                    }
                }
            },adjustments:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Adjustments).'
                    }
                }
            },nodays:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no number of days).'
                    }
                }
            },
        }
    });
    $('.overtime').hide();
    if($('#chkovertime').is(':checked')){
            $('.overtime').show();
        }
        else{
            $('.overtime').hide();
        }
        if($('#chkundertime').is(':checked')){
            $('.undertime').show();
        }
        else{
            $('.undertime').hide();
        }
        if($('#adjustments').is(':checked')){
            $('.adjustments').show();
        }
        else{
            $('.adjustments').hide();
        }
        $('#adjustments').on('click',function(){
        if($(this).is(':checked')){
            $('.adjustments').show();
            $('.adjustments').val("0");
        }
        else{
            $('.adjustments').hide();
            $('.adjustments').val("0");
        }
        
    });
    $('#chkovertime').on('click',function(){
        if($(this).is(':checked')){
            $('.overtime').show();
            $('.otnoofhours').val("0");
            $('.otpercent').val("0");
        }
        else{
            $('.overtime').hide();
            $('.otnoofhours').val("0");
            $('.otpercent').val("0");
        }
        
    });$('#chkundertime').on('click',function(){
        if($(this).is(':checked')){
            $('.undertime').show();
        }
        else{
            $('.undertime').hide();
        }
        
    });
    $('.formcompute').on('submit',function(e){
        e.preventDefault();
       if($('.employeeselect').val()==""||$('.icashadvance').val()==""||$('.iabsent').val()==""||$('.iundertime').val()==""){
            $('.formcompute').bootstrapValidator('validate');
        }else{
            if($('.payrollperiod').val()=="Monthly"){
                numberofdays = parseInt(15)-parseInt($('.iabsent').val());
                $('.pnumberofdays').text(numberofdays);
                rateperday = parseFloat($('#perdayfromform').val());
                //absent
                totalabsent = parseFloat($('.iabsent').val()) * rateperday;
                $('.pabsent').text("Absent: "+$('.iabsent').val()+"day(s)");
                $('.absentamount').text(parseFloat(totalabsent, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                //absent
                
                //undertime
                iundertimehour = 0;
                iundertimeminute = 0;
                if($('#chkundertime').is(':checked')){
                    iundertimehour = parseFloat($('.iundertimehour').val());
                    iundertimeminute = parseFloat($('.iundertimeminute').val());
                }
                else{
                    iundertimehour = 0;
                    iundertimeminute = 0;
                }
                iundertimehour = iundertimehour * parseFloat($('#hourforundertime').val());
                iundertimeminute = iundertimeminute * parseFloat($('#minutesforundertime').val());
                totalundertime = iundertimehour + iundertimeminute;
                $('.pundertimeamount').text(parseFloat(totalundertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.pundertimehm').text("Undertime : "+$('.iundertimehour').val()+"h"+$('.iundertimeminute').val()+"m");
                //undertime
                //overtime
                iovertimehour = 0;
                iovertimeminute = 0;
                if($('#chkovertime').is(':checked')){
                    iovertimehour = parseFloat($('.iovertimehour').val());
                    iovertimeminute = parseFloat($('.iovertimeminute').val());
                }
                else{
                    iovertimehour = 0;
                    iovertimeminute = 0;
                }
                iovertimehour = iovertimehour * parseFloat($('#hourforundertime').val());
                iovertimeminute = iovertimeminute * parseFloat($('#minutesforundertime').val());
                totalovertime = iovertimehour + iovertimeminute;
                $('.povertimeamount').text(parseFloat(totalovertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.povertimehm').text("Overtime : "+$('.iovertimehour').val()+"h"+$('.iovertimeminute').val()+"m");
                //overtime
                //advance
                cashadvance = parseFloat($('#cashadvancefromform').val().replace(',',''));
                $('.pcashadvance').text(parseFloat(cashadvance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());

                //advance
                //grosspay
                salary = parseFloat($('.basicpay').text().replace(',',''));
                allowance = parseFloat($('.allowance').text().replace(',',''));
                adjustments = parseFloat($('.adjustments').val().replace(',',''));
                $('.padjustments').text(parseFloat(adjustments, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                totalgrosspay = salary+allowance+totalovertime+adjustments;
                $('.pgrosspay').text(parseFloat(salary, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.ptotalgrosspay').text(parseFloat(totalgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                //grosspay
                //totaldeductions
                sssjud = parseFloat($('#sssjud').val());
                philjud = parseFloat($('#philjud').val());
                pagjud = parseFloat($('#pagjud').val());
                taxjud = parseFloat($('#taxjud').val());
                totaldeductions=0;
               /*if(salary>16666.5){
                    exceed = salary + totalovertime;
                    exceed = exceed - 16666.5;
                    exceedpercent = exceed * .25;
                    pinakatax=exceedpercent+taxjud;
                    
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + pinakatax;
                        $('.ptax').text(parseFloat(pinakatax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    
                    
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                }
               else if(salary>10416.5){
                    exceed = salary + totalovertime;
                    exceed = exceed - 10416.5;
                    exceedpercent = exceed * .20;
                    pinakatax=exceedpercent+taxjud;
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + pinakatax;
                        $('.ptax').text(parseFloat(pinakatax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                }
                else{*/
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + taxjud;
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
               /* }
                */
                
                //totaldeductions
            }
            else if($('.payrollperiod').val()=="Daily"){
                
                numberofdays = parseInt($('#noofdaysfromfrom').val())-parseInt($('.iabsent').val());
                $('.pnumberofdays').text(numberofdays);
                rateperday = parseFloat($('#perdayfromform').val());
                //absent
                totalabsent = parseFloat($('.iabsent').val()) * rateperday;
                $('.pabsent').text("Absent: "+$('.iabsent').val()+"day(s)");
                $('.absentamount').text(parseFloat(totalabsent, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                //absent
                
                //undertime
                iundertimehour = 0;
                iundertimeminute = 0;
                if($('#chkundertime').is(':checked')){
                    iundertimehour = parseFloat($('.iundertimehour').val());
                    iundertimeminute = parseFloat($('.iundertimeminute').val());
                }
                else{
                    iundertimehour = 0;
                    iundertimeminute = 0;
                }
                iundertimehour = iundertimehour * parseFloat($('#hourforundertime').val());
                iundertimeminute = iundertimeminute * parseFloat($('#minutesforundertime').val());
                totalundertime = iundertimehour + iundertimeminute;
                $('.pundertimeamount').text(parseFloat(totalundertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.pundertimehm').text("Undertime : "+$('.iundertimehour').val()+"h"+$('.iundertimeminute').val()+"m");
                //undertime
                //overtime
                iovertimehour = 0;
                iovertimeminute = 0;
                if($('#chkovertime').is(':checked')){
                    iovertimehour = parseFloat($('.iovertimehour').val());
                    iovertimeminute = parseFloat($('.iovertimeminute').val());
                }
                else{
                    iovertimehour = 0;
                    iovertimeminute = 0;
                }
                iovertimehour = iovertimehour * parseFloat($('#hourforundertime').val());
                iovertimeminute = iovertimeminute * parseFloat($('#minutesforundertime').val());
                totalovertime = iovertimehour + iovertimeminute;
                $('.povertimeamount').text(parseFloat(totalovertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.povertimehm').text("Overtime : "+$('.iovertimehour').val()+"h"+$('.iovertimeminute').val()+"m");
                //overtime
                //advance
                cashadvance = parseFloat($('#cashadvancefromform').val().replace(',',''));
                $('.pcashadvance').text(parseFloat(cashadvance, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());

                //advance
                //grosspay
                salary = rateperday * numberofdays;
                allowance = parseFloat($('.allowance').text().replace(',',''));
                adjustments = parseFloat($('.adjustments').val().replace(',',''));
                $('.padjustments').text(parseFloat(adjustments, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                totalgrosspay = salary+allowance+totalovertime+adjustments;
                $('.pgrosspay').text(parseFloat(salary, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                $('.ptotalgrosspay').text(parseFloat(totalgrosspay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                //grosspay
                //totaldeductions
                sssjud = parseFloat($('#sssjud').val());
                philjud = parseFloat($('#philjud').val());
                pagjud = parseFloat($('#pagjud').val());
                taxjud = parseFloat($('#taxjud').val());
                totaldeductions=0;
               /* if(salary>16666.5){
                    exceed = salary + totalovertime;
                    exceed = exceed - 16666.5;
                    exceedpercent = exceed * .25;
                    pinakatax=exceedpercent+taxjud;
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + pinakatax;
                        $('.ptax').text(parseFloat(pinakatax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                }
               else if(salary>10416.5){
                    exceed = salary + totalovertime;
                    exceed = exceed - 10416.5;
                    exceedpercent = exceed * .20;
                    pinakatax=exceedpercent+taxjud;
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + pinakatax;
                        $('.ptax').text(parseFloat(pinakatax, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                }
                else{*/
                    if($('#rangejus').val()=="25"){
                        totaldeductions = totalundertime + totalabsent + cashadvance + taxjud;
                    }
                    else{
                        totaldeductions = totalundertime + totalabsent + cashadvance + sssjud + philjud + pagjud;
                    }
                    $('.ptotaldeductions').text(parseFloat(totaldeductions, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    netpay = totalgrosspay - totaldeductions;
                    $('.pnetpay').text(parseFloat(netpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                /*}*/
                
                
                //totaldeductions
            }
            
        $('.btnsavess').show();
        }

    });
    $('.employeeselect').select2({
        placeholder:'<--Select-->'
    });
    loademployee();
    function loademployee(){
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectEmployeetoCompute/<?php echo $_GET["cutoff_id"]?>',
            dataType:'json',
            success:function(data){
                 option = $("<option></option>");
                    option.val("");
                    option.text("SELECT");
                    $('.employeeselect').append(option);
                $.each(data,function(key,val){
                    option = $("<option></option>");
                    option.val(val.employee_id);
                    option.text(val.fullname);
                    $('.employeeselect').append(option);
                });
            }
        });
    }
    $('.computeagain').on('click',function(){
        location.reload();
    });
    $('.employeeselect').on('change',function(){
        id = $(this).val();
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectSpecificEmployeetoCompute/'+id,
            dataType:'json',
            success:function(data){
                $.each(data,function(key,val){
                    $('.pfullname').text("Fullname : "+val.Lastname+", "+val.Firstname+" "+val.Middlename+" "+val.Suffix+" .").css({"font-weight":"bold"});
                    $('.pdepartment').text("Department : "+val.Department).css({"font-weight":"bold"});
                    $('.pposition').text("Position : "+val.Position).css({"font-weight":"bold"});
                   
                    $('.daccountnumber').text("Bank Account Number: "+val.Bank_account_number).css({"font-weight":"bold"});
                    $('.prateperdays').text(parseFloat(val.Rate_perday, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('#perdayfromform').val(val.Rate_perday);
                    $('.allowance').text(parseFloat(val.Allowance/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('.pallowance').text(parseFloat(val.Allowance/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('.basicpay').text(parseFloat(val.Salary/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                      $('#taxjud').val(val.TAX);
                    $('#philjud').val(val.PHILHEALTH_amount);
                    $('#pagjud').val(val.PAGIBIG_amount);
                    $('#sssjud').val(val.SSS_amount);
                    if($('#rangejus').val()=="25"){
                       $('.ptax').text(parseFloat(val.TAX, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }else{
                        $('.psss').text(parseFloat(val.SSS_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                        $('.pphilhealth').text(parseFloat(val.PHILHEALTH_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                        $('.ppagibig').text(parseFloat(val.PAGIBIG_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    valsalaryforundertime = parseFloat(val.Salary/30);
                     $('.prateperday').text("Rate per day : "+parseFloat(valsalaryforundertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()).css({"font-weight":"bold"});
                    hourforundertime = valsalaryforundertime/parseFloat(8);
                    minutesforundertime = hourforundertime/60;
                    $('#hourforundertime').val(hourforundertime);
                    $('#minutesforundertime').val(minutesforundertime);
                });
                
            },error:function(){
                alert("Opps. Server error!");
            }
        });
    });

    $('.payrollperiod').on('change',function(){
        if($(this).val()=="Monthly"){
            $('.monthlyperiod').show();
            $('.noofdays').hide();
        }
        if($(this).val()=="Daily"){
            $('.monthlyperiod').hide();
            $('.noofdays').show();
        }

    });
    $('.payrollperiod').trigger('change');
    $('.savenajudangpayslip').on('click',function(){
       prateperdays = $('.prateperdays').text().replace(',','');
       pnumberofdays = $('.pnumberofdays').text().replace(',','');
       povertimeamount = $('.povertimeamount').text().replace(',','');
       pallowance = $('.pallowance').text().replace(',','');
       padjustments = $('.padjustments').text().replace(',','');
       pgrosspay = $('.pgrosspay').text().replace(',','');
       ptotalgrosspay = $('.ptotalgrosspay').text().replace(',','');

       absentamount = $('.absentamount').text().replace(',','');
       pundertimeamount = $('.pundertimeamount').text().replace(',','');
       pcashadvance = $('.pcashadvance').text().replace(',','');
       psss = $('.psss').text().replace(',','');
       pphilhealth = $('.pphilhealth').text().replace(',','');
       ppagibig = $('.ppagibig').text().replace(',','');
       ptax = $('.ptax').text().replace(',','');
       ptotaldeductions = $('.ptotaldeductions').text().replace(',','');
       pnetpay = $('.pnetpay').text().replace(',','');
       absentday = $('.iabsent').val();
       othour = $('.iovertimehour').val();
       otmin = $('.iovertimeminute').val();
       uthour = $('.iundertimehour').val();
       utmin = $('.iundertimeminute').val();


       employee_id = $('.employeeselect').val();
       cutoff_id = $('#cutoff_id').val();
       swal({
                                  title: "Are you sure to save the Payslip?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Saved!", "", "success"); 
                                $('.confirm').on('click', function(e){
                            $.ajax({
                            url:'<?php echo base_url()?>Payroll/savePayslip',
                            data:{
                                prateperdays:prateperdays,
                                pnumberofdays:pnumberofdays,
                                povertimeamount:povertimeamount,
                                pallowance:pallowance,
                                padjustments:padjustments,
                                pgrosspay:pgrosspay,
                                ptotalgrosspay:ptotalgrosspay,
                                absentamount:absentamount,
                                pundertimeamount:pundertimeamount,
                                pcashadvance:pcashadvance,
                                psss:psss,
                                pphilhealth:pphilhealth,
                                ppagibig:ppagibig,
                                ptax:ptax,
                                ptotaldeductions:ptotaldeductions,
                                pnetpay:pnetpay,
                                employee_id:employee_id,
                                cutoff_id:cutoff_id,
                                absentday:absentday,
                                othour:othour,
                                otmin:otmin,
                                uthour:uthour,
                                utmin:utmin
                            },
                            type:'post',
                            success:function(){
                                location.reload();
                            },error:function(){
                            }
                            });
                                });
              } else {
                swal("Cancelled!", "", "error");
              }
            });
       
    });
    $(document).on('click','.deletenapayroll',function(){
        id= $(this).data('id');
        fullname= $(this).data('fullname');
         swal({
                                  title: "Are you sure to delete "+fullname+" from payslips?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Deleted!", "", "success"); 
                                $('.confirm').on('click', function(e){
                            $.ajax({
                            url:'<?php echo base_url()?>Payroll/deleteFrompayslips',
                            data:{
                                id:id,
                            },
                            type:'post',
                            success:function(){
                                location.reload();
                            },error:function(){
                            }
                            });
                                });
              } else {
                swal("Cancelled!", "", "error");
              }
            });
    });
});
</script>
