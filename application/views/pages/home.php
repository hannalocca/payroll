<!doctype html>
<html lang="en">
<head>
    

</head>
<body>

<div class="wrapper">
    

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2 !important; ">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content" >
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row">
                            <div class="timeline-left">
                            <div class="col-md-4 col-lg-3">
                            <div class="card card-skill" style="box-shadow: 5px 5px 5px 5px #B4ADAD;">
                            <div class="wrapper-img" style="height: 228px;">
                            <img class="card-img-top" src="<?php echo base_url()?>uploads/Employee.png" style="width: 100%;height: 230px;">
                            <div class="overlay">
                                <a href="<?php echo base_url()?>Employee"  class="icon" title="User Profile">Employee Information</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="col-md-4 col-lg-3">
                            <div class="card card-skill" style="box-shadow: 5px 5px 5px 5px #B4ADAD;">
                            <div class="wrapper-img" style="height: 228px;">
                            <img class="card-img-top" src="<?php echo base_url()?>uploads/Payroll.png" style="width: 100%;height: 230px;">
                            <div class="overlay">
                                <a href="<?php echo base_url()?>Payroll"  class="icon" title="User Profile">Payroll</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>

    

</html>
