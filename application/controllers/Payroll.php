<?php 
 class Payroll extends CI_Controller
 {
 	
 	function __construct(){
 		parent::__construct();
 		$this->load->model('payrollModel');
 		$this->load->model('settingsModel');
 	}

 	public function index(){
		if($this->session->userdata('session')){
			$data['cutoff'] = $this->payrollModel->get();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Payroll/CutoffPayroll',$data);
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function computePayroll(){
		if($this->session->userdata('session')){
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Payroll/Action/Payroll');
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function selectEmployeetoCompute($cutoff_id){
		$emps = $this->payrollModel->selectEmployeetoCompute($cutoff_id);
		echo json_encode($emps);
	}
	public function selectSpecificEmployeetoCompute($id){
		$result = $this->payrollModel->selectSpecificEmployeetoCompute($id);
		echo json_encode($result);
	}
	public function addCutoff($from_,$date_from,$to_,$date_to,$year){
		$this->payrollModel->addCutoff($from_,$date_from,$to_,$date_to,$year);
	}
	public function selectExistCutoff($from_,$date_from,$to_,$date_to,$year){
		$result = $this->payrollModel->selectExistCutoff($from_,$date_from,$to_,$date_to,$year);
		if($result){
			echo json_encode(array("result" => "exist"));
		}else{
			echo json_encode(array("result" => "notexist"));
		}

	}
	public function savePayslip(){
		$prateperdays = $_POST['prateperdays'];
		$pnumberofdays = $_POST['pnumberofdays'];
		$povertimeamount = $_POST['povertimeamount'];
		$pallowance = $_POST['pallowance'];
		$padjustments = $_POST['padjustments'];
		$pgrosspay = $_POST['pgrosspay'];
		$ptotalgrosspay = $_POST['ptotalgrosspay'];
		$absentamount = $_POST['absentamount'];
		$pundertimeamount = $_POST['pundertimeamount'];
		$pcashadvance = $_POST['pcashadvance'];
		$psss = $_POST['psss'];
		$pphilhealth = $_POST['pphilhealth'];
		$ppagibig = $_POST['ppagibig'];
		$ptax = $_POST['ptax'];
		$ptotaldeductions = $_POST['ptotaldeductions'];
		$pnetpay = $_POST['pnetpay'];
		$employee_id = $_POST['employee_id'];
		$absentday = $_POST['absentday'];
		$othour = $_POST['othour'];
		$otmin = $_POST['otmin'];
		$uthour = $_POST['uthour'];
		$utmin = $_POST['utmin'];
		$cutoff_id = $_POST['cutoff_id'];
		$save = $this->payrollModel->savePayslip($prateperdays,$pnumberofdays,$povertimeamount,$pallowance,$padjustments,$pgrosspay,$ptotalgrosspay,$absentamount,$pundertimeamount,$pcashadvance,$psss,$pphilhealth,$ppagibig,$ptax,$ptotaldeductions,$pnetpay,$employee_id,$cutoff_id,$absentday,$othour,$otmin,$uthour,$utmin);
	}
	public function selectPayrolled(){
		$id = $_POST['cutoff_id'];
		$result = $this->payrollModel->selectPayrolled($id);
		echo json_encode($result);
	}
	public function PrintPayslip(){
		$data['branches'] = $this->settingsModel->selectBranches();
		$data['cutoff'] = $this->payrollModel->get();
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('templates/navigation');
		$this->load->view('pages/Reports/Payslips/Payslips',$data);
		
	}
	public function GeneratePayslip(){
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('pages/Reports/Payslips/Action/PrintPayslip');
		
	}
	public function PrintPayslipSpecific(){
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('pages/Reports/PrintPayslipSpecific');
		
	}
	public function PrintAccountNumbers(){
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('pages/Reports/AccountNumbers/Action/PrintAccountNumbers');
		
	}
	public function selectPayrolledfinal(){
		$id = $_POST['cutoff_id'];
		$cat = $_POST['cat'];
		$branch = $_POST['branch'];
		$result = $this->payrollModel->selectPayrolledfinal($id,$cat,$branch);
		echo json_encode($result);
	}
	public function selectPayrolledfinalSpecific(){
		$id = $_POST['payroll_id'];
		$result = $this->payrollModel->selectPayrolledfinalSpecific($id);
		echo json_encode($result);
	}
	public function accountnumbers(){
		$id = $_POST['cutoff_id'];
		$cat = $_POST['cat'];
		$branch = $_POST['branch'];
		$result = $this->payrollModel->accountnumbers($id,$cat,$branch);
		echo json_encode($result);
	}
	public function accountnumbersSummary(){
		$id = $_POST['cutoff_id'];
		$cat = $_POST['cat'];
		$branch = $_POST['branch'];
		$result = $this->payrollModel->accountnumbersSummary($id,$cat,$branch);
		echo json_encode($result);
	}
	public function PayrollSummary(){
		$data['branches'] = $this->settingsModel->selectBranches();
		$data['cutoff'] = $this->payrollModel->get();
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('templates/navigation');
		$this->load->view('pages/Reports/PayrollSummary/PayrollSummary',$data);
		
	}
	public function AccNum(){
		$data['branches'] = $this->settingsModel->selectBranches();
		$data['cutoff'] = $this->payrollModel->get();
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('templates/navigation');
		$this->load->view('pages/Reports/AccountNumbers/AccNum',$data);
		
	}
	public function ViewPayrollSummary(){
		$this->load->view('templates/header');
		$this->load->view('templates/footer');
		$this->load->view('pages/Reports/PayrollSummary/Action/ViewPayrollSummary');
		
	}
	public function deleteFrompayslips(){
		$id = $_POST['id'];
		$this->payrollModel->deleteFrompayslips($id);
	}

	public function deleteCutfoff($cutoff_id){
			$this->payrollModel->deleteCutfoffpayroll($cutoff_id);
			$this->payrollModel->deleteCutfoff($cutoff_id);
			
	}
}
?>