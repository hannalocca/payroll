<?php
class Settings extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model('settingsModel');
	}
	public function Branches(){
		if($this->session->userdata('session')){
			$data['branches'] = $this->settingsModel->selectBranches();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Settings/Branch/Branches',$data);
		}else{
			$this->load->view('index');
		}
	}

	public function addBranch($name){
		$result = $this->settingsModel->saveBranch($name);
	}
}
?>