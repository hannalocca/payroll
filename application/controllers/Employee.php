<?php 
class Employee extends CI_Controller
{
	
	function __construct(){
		parent::__construct();
		$this->load->model('employeeModel');
		$this->load->model('settingsModel');
	}
	public function index(){
		if($this->session->userdata('session')){
			$data['emp'] = $this->employeeModel->get();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Employee/Employee',$data);
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function AddEmployee(){
		if($this->session->userdata('session')){
			$data['branches'] = $this->settingsModel->selectBranches();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Employee/Action/AddEmployee',$data);
		}
		else{
			$this->load->view('index');
		}
		
	}public function UpdateEmployee(){
		if($this->session->userdata('session')){
			$id = $_GET['emp_id'];
			$data['info'] = $this->employeeModel->getSpecific($id);
			$data['branches'] = $this->settingsModel->selectBranches();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Employee/Action/AddEmployee',$data);
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function ViewEmployee(){
		if($this->session->userdata('session')){
			$id = $_GET['emp_id'];
			$data['info'] = $this->employeeModel->getSpecific($id);
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/Employee/Action/ViewEmployee',$data);
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function deletes($id){
		$this->employeeModel->deletes($id);
		$this->employeeModel->deletesd($id);
	}
	public function changeEmpStatus($id,$stat){
		$this->employeeModel->changeEmpStatus($id,$stat);
	}
	public function saveEmployee(){
		$fname = ucwords($this->input->post('fname'));
		$mname = ucwords($this->input->post('mname'));
		$lname = ucwords($this->input->post('lname'));
		$suffix = ucwords($this->input->post('suffix'));
		$id = ucwords($this->input->post('id'));
		$birth = ucwords($this->input->post('birth'));
		$gender = ucwords($this->input->post('gender'));
		$address = ucwords($this->input->post('address'));
		$zip = ucwords($this->input->post('zip'));
		$mstatus = ucwords($this->input->post('mstatus'));
		$contact = ucwords($this->input->post('contact'));
		$position = ucwords($this->input->post('position'));
		$salary = str_ireplace(",", "", $this->input->post('salary')) ;
		$rate = str_ireplace(",", "", $this->input->post('rate')) ;
		$hired = ucwords($this->input->post('hired'));
		$sssnum = ucwords($this->input->post('sssnum'));
		$philnum = ucwords($this->input->post('philnum'));
		$pagnum = ucwords($this->input->post('pagnum'));
		$sssamount = str_ireplace(",", "", $this->input->post('sssamount'));
		$philamount =str_ireplace(",", "", $this->input->post('philamount')) ;
		$pagamount = str_ireplace(",", "", $this->input->post('pagamount')) ;
		$taxamount = str_ireplace(",", "", $this->input->post('taxamount')) ;
		$taxnum = ucwords($this->input->post('taxnum'));
		$emp_id = ucwords($this->input->post('emp_id'));
		$department = ucwords($this->input->post('department'));
		$branch = $this->input->post('branch');
		$allowance =str_ireplace(",", "", $this->input->post('allowance')) ;
		$bank = ucwords($this->input->post('bankacc'));
		$save = $this->employeeModel->saveEmployee($emp_id,$fname,$mname,$lname,$suffix,$id,$birth,$gender,$address,$zip,$mstatus,$contact,$position,$salary,$rate,$hired,$sssnum,$philnum,$pagnum,$sssamount,$philamount,$pagamount,$taxamount,$taxnum,$allowance,$bank,$department,$branch);
		if($save){
			echo json_encode(array('save' => "ok" ));
		}
		else{
			echo json_encode(array('save' => "not" ));
		}
	}
}
?>