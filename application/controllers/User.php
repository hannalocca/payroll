<?php 
class User extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('userModel');
	}
	public function index(){
		if($this->session->userdata('session')){
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/home');
		}
		else{
			$this->load->view('index');
		}
		
	}
	public function Login($user,$password){
		$result = $this->userModel->Login($user,$password);
		if($result){
			$this->session->set_userdata('session',$result);
			echo json_encode(array('message' => "ok"));
		}
		else{
			echo json_encode(array('message' => "not"));
		}
	}
	public function Logout(){
		$this->session->unset_userdata('session');
		redirect('/');
	}
	public function Users(){
		if($this->session->userdata('session')){
			$data['users'] = $this->userModel->selectUsers();
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/User/Users',$data);
		}
		else{
			$this->load->view('index');
		}
	}
	public function addUser(){
		if($this->session->userdata('session')){
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
			$this->load->view('templates/navigation');
			$this->load->view('pages/User/AddUser');
		}
		else{
			$this->load->view('index');
		}
	}

	public function selectEmployeeforAddUsers(){
		$result = $this->userModel->selectEmployeeforAddUsers();
		echo json_encode($result);
	}
	public function saveUser(){
		$employee_id = $this->input->post('employee');
		$usertype = $this->input->post('usertype');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$result = $this->userModel->saveUser($employee_id,$usertype,$username,$password);
		echo json_encode($result);
	}
	public function deleteUser($id){
		$this->userModel->deleteUser($id);
	}
	public function isActive($id,$stats){
		$this->userModel->isActive($id,$stats);
	}
	public function selectusernameifexist($username){
		$result = $this->userModel->selectusernameifexist($username);
		if($result){
			echo json_encode(array('message' => 'ok' ));
		}
		else{
			echo json_encode(array('message' => 'not' ));
		}
	}
}
?>